# nodeのltsを指定
FROM node:12.13.0-alpine
WORKDIR /var/www/app
#COPY ./package*.json ./

RUN apk update && \
    npm install && \
    npm install -g npm && \
#    npm install -g vue-cli
    npm install -g create-nuxt-app

#COPY . .
#RUN npm run build
#CMD ["/bin/ash"]
CMD ["npm", "run", "serve"]

